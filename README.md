# ttv-rs
Attempt at unifying the [Twitch API](https://dev.twitch.tv/docs/) for the Rust ecosystem.

## Why?
This collection of crates was born out of a desire to create a ["One library to rule them all!"](https://xkcd.com/927/)
to interact with the Twitch API using Rust. Initially I thought every sub-crate would likely depend
on a different already-existing crate on crates.io, or implement the features itself if none were
suitable. However, since beginning this project, I have realized that the [twitch-api2](https://crates.io/crates/twitch_api2)
crate, which I originally believed was only intending to cover Helix and TMI, is actually aspiring 
to cover most of the other endpoints as well eventually (see [this issue](https://github.com/Emilgardis/twitch_api2/issues/27).
Thus, this library is currently only providing a way to combine both [twitchchat](https://crates.io/crates/twitchchat) 
and [twitch-api2](https://crates.io/crates/twitch_api2) in one dependency, with shorter names 😛

I'm still deciding whether re-exporting `twitch_api2` is really the best way to achieve my goals 
below, but it is looking promising. However, even if that crate ends up covering all of the
endpoints on its own, I may end up trying to provide additional convenience features on top of it,
such as functions that automatically pick the best endpoint for certain requests, chat features that
utilize REST or pubsub endpoints, a Webhooks server, UI, database integration, etc...


## Goals
- Cover the entirety of all API's listed in Twitch's [documentation](https://dev.twitch.tv/docs/)
- Cover several of the useful [unofficial endpoints](https://thomassen.sh/twitch-api-endpoints/) 
  that are known now or discovered in the future.
- **Match the organization of Twitch's documentation as closely as possible.**
    - This makes it easier for users of the crate to find and cross-reference documentation and make
      use of tutorials for technologies other than Rust, as well as making it easier for contributors
      to add documentation.
- Flexibility
    - As often as reasonable, this crate should be implementation-agnostic (e.g. able to support
      various async runtimes, networking implementations, serialization libraries, etc.)
    - Users should be able to avoid compiling features they don't need. 
        - However, a sane set of default features is still ***critical*** in my opinion. Currently
          all **active** and **deprecated** API endpoints are enabled by default, but **unofficial**
          endpoints are not.

Note that currently I am very impressed with Museun's [twitchchat](https://crates.io/crates/twitchchat)
crate, and would love it if all of the other modules mimic its API and interoperate well with it. E.g.,
you should be able to use the same async executor for all endpoints.

Similarly, I am a fan of the [sans-io](https://sans-io.readthedocs.io/) approach to library design, 
although it's also nice if a library provides optional integration with other libraries to actually
implement the IO, for less boilerplate on the end-user's part.

Also, as I stated, my ideas are heavily informed by the design of 
[Twitch4j](https://github.com/twitch4j/twitch4j); however, I won't be following it precisely. I have
some of my own opinions of how things should be organized as well. 

## Progress
#### Active Twitch API's
| Feature      | Status           | Current Implementation                                                                      |
|--------------|------------------|---------------------------------------------------------------------------------------------|
| Auth         | …  Testing       | [twitch_oauth2](https://crates.io/crates/twitch_oauth2)                                     |
| Chat         | …  Testing       | [twitchchat](https://crates.io/crates/twitchchat)                                           |
| Helix        | …  Testing       | [twitch_api2](https://crates.io/crates/twitch_api2) (with only `helix` feature)             |
| Extensions   | ❌ Unimplemented | None                                                                                        |
| PubSub       | …  Testing       | [twitch_api2](https://crates.io/crates/twitch_api2) (with only `pubsub` feature)            |
| Webhooks     | ❌ Unimplemented | None                                                                                        |

#### Deprecated Twitch API's
| Feature      | Status           | Current Implementation                                                                      |
|--------------|------------------|---------------------------------------------------------------------------------------------|
| v5 (Kraken)  | ❌ Unimplemented | ([libtwitch-rs](https://crates.io/crates/libtwitch-rs) is currently not license-compatible) |
| Legacy Drops | ❌ Unimplemented | None                                                                                        |

#### Unofficial Twitch API's
| Feature      | Status           | Current Implementation                                                                      |
|--------------|------------------|---------------------------------------------------------------------------------------------|
| TMI          | … Testing        | [twitch_api2](https://crates.io/crates/twitch_api2) (with only `tmi` feature)               |
| GraphQL      | ❌ Unimplemented | None                                                                                        |

## Contributing
Currently I am mostly looking for feedback and input on my vision and ideas, as well as possible
existing crates that may fit well into this project. PR's are welcome, but beware that I may
be very opinionated about how things are organized and implemented, and may be changing my mind
often for the time being.

## License
Licensed under MIT and Apache 2.0